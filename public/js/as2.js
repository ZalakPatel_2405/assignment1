//var passwordRegex =  /^[0-9A-Za-z]{6,16}$/;
//var cardRegex = /^[0-9]{4}[\-][0-9]{4}[\-][0-9]{4}[\-][0-9]{4}$/;
//var emailRegex = /^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/;

function formSubmit(){

    return true;
    
    var name = document.getElementById('name').value;
    var adress = document.getElementById('adress').value;
    var city = document.getElementById('city').value;
    var province = document.getElementById('province').value;
    var email = document.getElementById('email').value;
    var password = document.getElementById('password').value;
    var confirmPassword = document.getElementById('confirmPassword').value;
    var MangoChunks = document.getElementById('quantity1').value;
    var FreshTopica = document.getElementById('quantity2').value;
    var LemonTea = document.getElementById('quantity3').value;
    var cardNumber = document.getElementById('cardNumber').value;

    var payment = document.getElementsByName('payment');
   
    var errors = '';
//Name
    if(name == '' ){
        errors = `Name is Required. <br>`;
        document.getElementById('nameError').innerHTML = errors;
    }
    else{
        document.getElementById('nameError').innerHTML = '';
    }
//Adress
    if(adress == ''){
        errors = `Adress is Required. <br>`;
        document.getElementById('adressError').innerHTML = errors;
    }
    else{
        document.getElementById('adressError').innerHTML = '';
    }
//City
    if(city == ''){
        errors = `City is Required. <br>`;
        document.getElementById('cityError').innerHTML = errors;
    }
    else{
        document.getElementById('cityError').innerHTML = '';
    }
//Province
    if(province == ''){
        errors = `Please Select Province. <br>`;
        document.getElementById('provinceError').innerHTML = errors;
    }
    else{
        document.getElementById('provinceError').innerHTML = '';
    }
//Email
    if(email != ''){
        if(!emailRegex.test(email)){
            errors = `Email is not in Correct Format. <br>`;
            document.getElementById('emailError').innerHTML = errors;
        }
        else{
            document.getElementById('emailError').innerHTML = '';
        }
    }
    else{
         errors = `Email is Required. <br>`;
         document.getElementById('emailError').innerHTML = errors;
    }
//Password 
    if(password.length < 6){
        errors = `Password should be 6 characters or more. <br>`;
        document.getElementById('passwordError').innerHTML = errors;
    }
    else{
        document.getElementById('passwordError').innerHTML = '';
    }
//Confirm Password
    if(confirmPassword != password){
        errors = `Password is not Matching <br>`;
        document.getElementById('confirmPasswordError').innerHTML = errors;
    }
    else{
        document.getElementById('confirmPasswordError').innerHTML = '';
    }
//Payment Method 
    paymentChecked = false;
    for(var i = 0; i<payment.length; i++){
        if(payment[i].checked){
            paymentChecked = i;
        }
    }
    if(paymentChecked === false){
        errors = `Please select Payment method. <br>`;
        document.getElementById('paymentError').innerHTML = errors;
    }
    else{
        payment = payment[paymentChecked];
        document.getElementById('paymentError').innerHTML = '';
    }
//Card Number
    if( cardNumber != ''){
        if(!cardRegex.test(cardNumber)){
            errors = `CardNumber is not in correct format. <br>`;
            document.getElementById('cardNumberError').innerHTML = errors;
        }
        else{
            document.getElementById('cardNumberError').innerHTML = '';
        }
    }
    else{
         errors = `Card Number is Required. <br>`;
         document.getElementById('cardNumberError').innerHTML = errors;
    }
//errors   
    if(errors != ''){
        document.getElementById('formResult').innerHTML = '';  
    }
//Output
    else{
        return true;// allows the form to be submitted if there is no errors
        document.getElementById('errors').innerHTML = '';
        var myOutput = '';

        myOutput += `-------------Receipt:------------- <br>
                    Name: ${name} <br>
                    Adress: ${adress} <br>
                    City: ${city} <br>
                    Province: ${province} <br>
                    Email: ${email} <br>`

        var cost = 0;
        var mangoChunksCost = 8;
        var freshTopicaCost = 10;
        var lemonTeaCost = 6;
        if(MangoChunks != ''){
           cost += MangoChunks * mangoChunksCost ;
           myOutput += ` Mango Chuks = ${MangoChunks * mangoChunksCost} <br>`
        }
        if(FreshTopica  != ''){
            cost += FreshTopica * freshTopicaCost;
            myOutput += `Fresh Topica = ${FreshTopica * freshTopicaCost} <br>`
        } 
        if(LemonTea != ''){
            cost += LemonTea * lemonTeaCost;
            myOutput += `Lemon Tea = ${LemonTea * lemonTeaCost} <br>`
        }

        var totalCost = cost;
        if(totalCost<10){
            document.getElementById('errors').innerHTML = 'Your Purchase should be more than 10$';
            document.getElementById('formResult').innerHTML = '';
        }
        else{
            if(province == `ON`){taxTotalCost = totalCost * 1.13}
            if(province == `NL`){taxTotalCost = totalCost * 1.15}
            if(province == `PE`){taxTotalCost = totalCost * 1.15}
            if(province == `NS`){taxTotalCost = totalCost * 1.15}
            if(province == `NB`){taxTotalCost = totalCost * 1.15}
            if(province == `QC`){taxTotalCost = totalCost * 1.149}
            if(province == `MB`){taxTotalCost = totalCost * 1.12}
            if(province == `SK`){taxTotalCost = totalCost * 1.11}
            if(province == `AB`){taxTotalCost = totalCost * 1.05}
            if(province == `BC`){taxTotalCost = totalCost * 1.12}
            if(province == `YT`){taxTotalCost = totalCost * 1.05}
            if(province == `NT`){taxTotalCost = totalCost * 1.05}
            if(province == `NU`){taxTotalCost = totalCost * 1.05}
       
            myOutput += `Total Cost: ${taxTotalCost.toFixed(2)} <br> `; 
        document.getElementById('formResult').innerHTML = myOutput;
        }
    }
    return false;  
}