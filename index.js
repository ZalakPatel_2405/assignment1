// Server site Programming

//import the dependecies
const express = require('express');
const path = require('path');
const bodyparser = require('body-parser');
const { check, validationResult } = require('express-validator');

// set up global variables to use package
var myApp = express();
myApp.use(bodyparser.urlencoded({ extended: false }));

//set up path to public folders and views
myApp.set('views', path.join(__dirname, 'views'));//relative path

//set up the path for public stuff like css, client side JS, images etc.
myApp.use(express.static(__dirname + '/public'));

//define the view engine
myApp.set('view engine', 'ejs');

// ------------validation fuction---------------
var passwordRegex = /^[0-9A-Za-z]{6,16}$/;
var cardRegex = /^[0-9]{4}[\-][0-9]{4}[\-][0-9]{4}[\-][0-9]{4}$/;
var phoneRegex = /^[1-9]{1}[0-9]{9}$/;

// fuction to check a string using regular expression
function checkRegex(userInput, regex) {
    if (regex.test(userInput)) {
        return true;
    }
    else {
        return false;
    }
}
// Custom phone validation function
function customPasswordValidation(value) {
    if (!checkRegex(value, passwordRegex)) {
        throw new Error('Password should be 6 characters or more.')
    }
    return true;
}
function customCardValidation(value) {
    if (!checkRegex(value, cardRegex)) {
        throw new Error('CardNumber should be in correct format.')
    }
    return true;
}
function customPhoneValidation(value) {
    if (!checkRegex(value, phoneRegex)) {
        throw new Error('Phone Number should be in correct format.')
    }
    return true;
}
function customConfirmPassword(value, { req }) {
    var password = req.body.password;
    var confirmPassword = req.body.confirmPassword;
    if (confirmPassword != password) {
        throw new Error('Password is not Matching.')
    }
    return true;
}
function customTotalCheck(value, { req }) {
    var cost1 = 0;
    var cost2 = 0;
    var cost3 = 0;

    var mangoChunksCost = 8;
    var freshTopicaCost = 10;
    var lemonTeaCost = 6;

    if (value != '') {
        cost1 = req.body.quantity1 * mangoChunksCost;
        // myOutput += ` Mango Chuks = ${MangoChunks * mangoChunksCost} <br>`
    }
    if (req.body.quantity2 != '') {
        cost2 = req.body.quantity2 * freshTopicaCost;
        // myOutput += `Fresh Topica = ${FreshTopica * freshTopicaCost} <br>`
    }
    if (req.body.quantity3 != '') {
        cost3 = req.body.quantity3 * lemonTeaCost;
        //myOutput += `Lemon Tea = ${LemonTea * lemonTeaCost} <br>`
    }

    var totalCost = cost1 + cost2 + cost3;

    if (totalCost < 10) {
        throw new Error('Purchase shoulde be more then 10$.')
    }
    return true;
}
function customPaymentCheck(value) {
    if (value == null || value == 'undefined') {
        throw new Error('Select Payment method.')
    }
    return true;
}

// handle the HTTP request/ define routes

//home page
myApp.get('/', function (req, res) {
    res.render('form');
});

myApp.post('/', [
    check('name', 'Name is Required.').notEmpty(),
    check('address', 'Adress is Required.').notEmpty(),
    check('city', 'City is Required.').notEmpty(),
    check('province', 'Province is Required.').notEmpty(),
    check('email', 'Email sholud be in correct Format.').isEmail(),
    check('phoneNumber', '').custom(customPhoneValidation),
    check('password', '').custom(customPasswordValidation),
    check('confirmPassword', '').custom(customConfirmPassword),
    check('cardNumber', '').custom(customCardValidation),
    check('quantity1').custom(customTotalCheck),
    check('payment').custom(customPaymentCheck)

], function (req, res) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.render('form', {
            errors: errors.array()
        })
    }
    else {
        var name = req.body.name;
        var address = req.body.address;
        var city = req.body.city;
        var province = req.body.province;
        var email = req.body.email;
        var phoneNumber = req.body.phoneNumber;
        var password = req.body.password;
        var confirmPassword = req.body.confirmPassword;
        var quantity1 = req.body.quantity1;
        var quantity2 = req.body.quantity2;
        var quantity3 = req.body.quantity3;
        var payment = req.body.payment;
        var cardNumber = req.body.cardNumber;

        var cost1 = 0;
        var cost2 = 0;
        var cost3 = 0;
        
        var mangoChunksCost = 8;
        var freshTopicaCost = 10;
        var lemonTeaCost = 6;
        var taxTotalCost = 0;
        if (quantity1 != '') {
            cost1 += quantity1 * mangoChunksCost;
            // myOutput += ` Mango Chuks = ${MangoChunks * mangoChunksCost} <br>`
        }
        if (quantity2 != '') {
            cost2 += quantity2 * freshTopicaCost;
            // myOutput += `Fresh Topica = ${FreshTopica * freshTopicaCost} <br>`
        }
        if (quantity3 != '') {
            cost3 += quantity3 * lemonTeaCost;
            //myOutput += `Lemon Tea = ${LemonTea * lemonTeaCost} <br>`
        }

        var totalCost = cost1 + cost2 + cost3;

        if (province == 'ON') { taxTotalCost = totalCost * 1.13 }
        if (province == 'NL') { taxTotalCost = totalCost * 1.15 }
        if (province == 'PE') { taxTotalCost = totalCost * 1.15 }
        if (province == 'NS') { taxTotalCost = totalCost * 1.15 }
        if (province == 'NB') { taxTotalCost = totalCost * 1.15 }
        if (province == 'QC') { taxTotalCost = totalCost * 1.149 }
        if (province == 'MB') { taxTotalCost = totalCost * 1.12 }
        if (province == 'SK') { taxTotalCost = totalCost * 1.11 }
        if (province == 'AB') { taxTotalCost = totalCost * 1.05 }
        if (province == 'BC') { taxTotalCost = totalCost * 1.12 }
        if (province == 'YT') { taxTotalCost = totalCost * 1.05 }
        if (province == 'NT') { taxTotalCost = totalCost * 1.05 }
        if (province == 'NU') { taxTotalCost = totalCost * 1.05 }
  
        var pageData = {
            zpname: name,
            zpaddress: address,
            zpcity: city,
            zpprovince: province,
            zpemail: email,
            zpphoneNumber: phoneNumber,
            zppassword: password,
            zpconfirmPassword: confirmPassword,
            zpquantity1: quantity1,
            zpquantity2: quantity2,
            zpquantity3: quantity3,
            zppayment: payment,
            zpcardNumber: cardNumber,
            cost1: cost1,
            cost2: cost2,
            cost3: cost3,
            zptaxtotalCost: taxTotalCost
        }
        res.render('form', pageData);

    }
});

// start the server and listen to a port

myApp.listen(8080);

console.log('Go to Browser');